import partnerToolsTemplate from './partner-tools.html!text';
import PartnerToolsController from './partnerToolsController';

export default class RouteConfig {

    constructor($routeProvider) {

        $routeProvider
            .when
            (
                '/',
                {
                    template: partnerToolsTemplate,
                    controller: PartnerToolsController,
                    controllerAs: 'controller'
                }
            );

    }

}

RouteConfig.$inject = [
    '$routeProvider'
];