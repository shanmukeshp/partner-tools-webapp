import {IdentityServiceSdkConfig} from 'identity-service-sdk';
import {PartnerRepServiceSdkConfig} from 'partner-rep-service-sdk';
import {SessionManagerConfig} from 'session-manager';

export default class Config {

    _identityServiceSdkConfig:IdentityServiceSdkConfig;

    _partnerRepServiceSdkConfig:PartnerRepServiceSdkConfig;

    _sessionManagerConfig:SessionManagerConfig;

    /**
     * @param {IdentityServiceSdkConfig} identityServiceSdkConfig
     * @param {PartnerRepServiceSdkConfig} partnerRepServiceSdkConfig
     * @param {SessionManagerConfig} sessionManagerConfig
     */
    constructor(identityServiceSdkConfig:IdentityServiceSdkConfig,
                partnerRepServiceSdkConfig:PartnerRepServiceSdkConfig,
                sessionManagerConfig:SessionManagerConfig) {

        if (!identityServiceSdkConfig) {
            throw new TypeError('identityServiceSdkConfig required');
        }
        this._identityServiceSdkConfig = identityServiceSdkConfig;

        if (!partnerRepServiceSdkConfig) {
            throw new TypeError('partnerRepServiceSdkConfig required');
        }
        this._partnerRepServiceSdkConfig = partnerRepServiceSdkConfig;

        if (!sessionManagerConfig) {
            throw new TypeError('sessionManagerConfig');
        }
        this._sessionManagerConfig = sessionManagerConfig;

    }

    get identityServiceSdkConfig() {
        return this._identityServiceSdkConfig;
    }

    get partnerRepServiceSdkConfig() {
        return this._partnerRepServiceSdkConfig;
    }

    get sessionManagerConfig() {
        return this._sessionManagerConfig;
    }
}