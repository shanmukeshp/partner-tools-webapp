import angular from 'angular';
import 'angular-route';
import 'add-partner-rep-modal';
import 'angular-messages';
import 'angular-bootstrap';
import 'header';
import 'footer';
import 'bootstrap';
import "bootstrap/css/bootstrap.css!css"
import "./app.css!css";
import "./partner-tools.css!css";
import Iso3166Sdk from 'iso-3166-sdk';
import ConfigFactory from './configFactory';
import Config from './config';
import configData from './configData.json!json';
import RouteConfig from './routeConfig';
import IdentityServiceSdk from 'identity-service-sdk';
import PartnerRepServiceSdk from 'partner-rep-service-sdk';
import SessionManager from 'session-manager';

angular
    .module(
        'partnerRepToolsWebApp.module',
        [
            'ngRoute',
            'addPartnerRepModal.module',
            'ngMessages',
            'ui.bootstrap',
            'header.module',
            'footer.module'
        ])
    .constant(
        'iso3166Sdk',
        new Iso3166Sdk()
    )
    .factory(
        'config',
        () => ConfigFactory.construct(configData)
    )
    .factory(
        'identityServiceSdk',
        [
            'config',
            config => new IdentityServiceSdk(config.identityServiceSdkConfig)
        ]
    )
    .factory(
        'partnerRepServiceSdk',
        [
            'config',
            config => new PartnerRepServiceSdk(config.partnerRepServiceSdkConfig)
        ]
    )
    .factory(
        'sessionManager',
        [
            'config',
            config => new SessionManager(config.sessionManagerConfig)
        ]
    ).config(
    [
        '$routeProvider',
        $routeProvider => new RouteConfig($routeProvider)
    ]
);