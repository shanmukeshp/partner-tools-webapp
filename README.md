## Description
Partner tools web app
 
## Features

##### Add Partner Rep
Allows the currently logged in partner rep to add a rep of their partner.

## Install

```PowerShell
# installs dependent node & jspm packages, then bundles the app
npm install
```

## Configuration
Configuration of the web app is obtained at runtime from [configData.json](src/configData.json)